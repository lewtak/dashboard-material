import React, { useState, useEffect } from 'react';
import _isEmpty from 'lodash/isEmpty';
import _isUndefined from 'lodash/isUndefined';
import { Redirect, Switch, Route } from 'react-router-dom';
import constants from '../../constants';
import AddressDetails from 'components/CheckoutForm/AddressDetails';
import CardDetails from 'components/CheckoutForm/CardDetails';
import OrderSummary from 'components/CheckoutForm/OrderSummary';
import { useHistory } from 'react-router-dom';
import PageTitle from 'components/PageTitle';
import Dialog from 'components/Dialog';
import withCheckoutForm from 'components/CheckoutForm/withCheckoutForm';
import CheckoutForm from 'components/CheckoutForm';

const arrSteps = [
  {
    id: '77abc',
    slug: 'shipping-address',
  },
  {
    id: '78abc',
    slug: 'payments-details',
  },
  {
    id: '79abc',
    slug: 'order-summary',
  },
];

const stepComponentById = {
  '77abc': {
    component: (props) => <AddressDetails {...props} />,
  },
  '78abc': {
    component: (props) => <CardDetails {...props} />,
  },
  '79abc': {
    component: (props) => <OrderSummary {...props} />,
  },
};
const { APP } = constants;
const Checkout = ({
  match,
  openPopup,
  funcSetClosePopup,
  resetCheckoutForm,
}) => {
  const [checkoutSteps, setCheckoutSteps] = useState([]);

  useEffect(() => {
    setCheckoutSteps(arrSteps);
    return () => {
      console.log('callback');
    };
  }, []);
  const { push } = useHistory();

  const checkoutFormStep =
    !_isEmpty(checkoutSteps) && checkoutSteps.length > 0 && checkoutSteps[0];

  const firstStepSlug =
    !_isUndefined(checkoutFormStep) &&
    checkoutFormStep &&
    checkoutFormStep.slug;

  const handleGotoNextStep = (currentIndex) => {
    const lastCheckoutFormStep = currentIndex === arrSteps.length - 1;
    if (!lastCheckoutFormStep) {
      const nextStep = arrSteps[currentIndex + 1].slug;
      push(`${match.url}/${nextStep}`);
    }
  };

  const handleGotoPreviousStep = (currentIndex) => {
    const previousStep = arrSteps[currentIndex - 1].slug;
    push(`${match.url}/${previousStep}`);
  };

  const handleClosePopUp = () => {
    funcSetClosePopup();
  };

  const closeDialogBackToStart = () => {
    funcSetClosePopup();
    push(APP.DASHBOARD);
    resetCheckoutForm();
  };

  return (
    <>
      <PageTitle title='Checkout' />

      {!_isEmpty(checkoutSteps) && checkoutSteps.length > 0 && (
        <Switch>
          <Redirect
            from={APP.CHECKOUT}
            exact
            to={`${APP.CHECKOUT}/${firstStepSlug}`}
          />
          {checkoutSteps.map((step, index) => {
            const Step = stepComponentById[step.id].component;

            return (
              <Route
                key={step.id}
                path={`${match.url}/${step.slug}`}
                render={(props) => (
                  <Step
                    {...props}
                    index={index}
                    handleGotoNextStep={handleGotoNextStep}
                    handleGotoPreviousStep={handleGotoPreviousStep}
                  />
                )}
              />
            );
          })}
        </Switch>
      )}
      <Dialog
        handleClosePopUp={handleClosePopUp}
        openPopup={openPopup}
        title={'Check your details before you send!'}
      >
        <CheckoutForm closeDialogBackToStart={closeDialogBackToStart} />
      </Dialog>
    </>
  );
};

Checkout.propTypes = {};

export default withCheckoutForm(Checkout);
