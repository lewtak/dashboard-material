import { ON_INPUT_CHANGE, ON_SUBMIT_CHECKOUT_SUMMARY } from '../types';
export const objInitialState = {
  shippingAddress: {
    firstName: '',
    lastName: '',
    address1: '',
    address2: '',
    city: '',
  },

  cardDetails: {
    email: '',
    cardNumber: '',
    description: '',
  },

  orderSummary: {
    notes: '',
  },

  objDataSummary: {},
  openPopup: false,
};

/**
 * @param {object} state Current Redux state
 * @param {object} action
 * @returns {object} New state
 */
const reducerCheckout = (state = objInitialState, action) => {
  const { formData, openPopup } = action.payload || {};
  switch (action.type) {
    case ON_INPUT_CHANGE:
      const { name, value } = action.payload;
      return {
        ...state,
        initialValues: { ...state.initialValues, [name]: value },
      };
    case ON_SUBMIT_CHECKOUT_SUMMARY:
      const { dataSummary } = action.payload;

      return {
        ...state,
        dataSummary,
      };

    case 'SET_ADDRESS_DETAILS':
      return {
        ...state,
        shippingAddress: { ...formData },
      };
    case 'SET_CARD_DETAILS':
      return {
        ...state,
        cardDetails: { ...formData },
      };
    case 'SET_SUMMARY_NOTES':
      return {
        ...state,
        orderSummary: {
          ...formData,
        },
      };
    case 'SAVE_CHECKOUT_DETAILS':
      const { summaryData } = action.payload;

      return {
        ...state,
        objDataSummary: { ...summaryData },
      };
    case 'OPEN_DIALOG_POPUP':
      return {
        ...state,
        openPopup,
      };

    case 'CLOSE_DIALOG_POPUP':
      return {
        ...state,
        openPopup,
      };

    case 'RESET_CHECKOUT_FORM':
      return {
        ...state,
        shippingAddress: { ...objInitialState.shippingAddress },
        cardDetails: { ...objInitialState.cardDetails },
        orderSummary: { ...objInitialState.orderSummary },
      };

    default:
      return state;
  }
};

export default reducerCheckout;
