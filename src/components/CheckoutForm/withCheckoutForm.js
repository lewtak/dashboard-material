import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  setAddressDetails,
  setCardDetails,
  setSummaryWithNotes,
  saveCheckoutDetails,
} from 'redux/CheckoutForm/actions/ActionSetSummaryData';
import {
  setOpenPopup,
  setClosePopup,
} from 'redux/CheckoutForm/actions/ActionsDialog';
import { resetCheckoutForm } from 'redux/CheckoutForm/actions/ActionResetCheckoutForm';

const withCheckoutForm = (WrappedComponent) => {
  class HOC extends Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  const mapDispatchToProps = {
    funcSetAddress: setAddressDetails,
    funcSetCardDetails: setCardDetails,
    funcSetSummaryWithNotes: setSummaryWithNotes,
    funcSaveCheckoutDetails: saveCheckoutDetails,
    funcSetOpenPopup: setOpenPopup,
    funcSetClosePopup: setClosePopup,
    resetCheckoutForm,
  };
  /**
   *
   * @param {object} state Redux state
   * @param {object} ownProps Props passed directly to the component
   */
  const mapStateToProps = (state, ownProps) => {
    const {
      shippingAddress,
      cardDetails,
      orderSummary,
      objDataSummary,
      openPopup,
    } = state.checkoutState;
    return {
      ...ownProps,
      shippingAddress,
      cardDetails,
      orderSummary,
      objDataSummary,
      openPopup,
    };
  };

  return connect(mapStateToProps, mapDispatchToProps)(HOC);
};

export default withCheckoutForm;
