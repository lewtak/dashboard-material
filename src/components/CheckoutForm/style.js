import { makeStyles } from '@material-ui/styles';

export default makeStyles((theme) => ({
  formDetails: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-betwwen',
    alignItems: 'center',
  },

  formControlWrapper: {
    width: '90%',
    padding: '0 15px',
  },
}));
