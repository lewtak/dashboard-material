import React from 'react';

import Routes from 'routes';
import { Switch } from 'react-router-dom';
const App = () => {
  return (
    <Switch>
      <Routes />
    </Switch>
  );
};

App.defaultProps = {};

export default App;
