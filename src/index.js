import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import App from 'components/App';
import configureStore from 'redux/store';
import { ThemeProvider } from '@material-ui/styles';
import { CssBaseline } from '@material-ui/core';

import Themes from './themes';
import { LayoutProvider } from './context/LayoutContext';
import { UserProvider } from './context/UserContext';

const store = configureStore();

ReactDOM.render(
  <LayoutProvider>
    <Provider store={store}>
      <UserProvider>
        <ThemeProvider theme={Themes.default}>
          <CssBaseline />
          <Router>
            <App />
          </Router>
        </ThemeProvider>
      </UserProvider>
    </Provider>
  </LayoutProvider>,
  document.getElementById('root')
);
