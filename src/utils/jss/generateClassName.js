import { createGenerateClassName } from '@material-ui/core/styles';

export const generateClassName = createGenerateClassName({
  seed: 'app1',
});
