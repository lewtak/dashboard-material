const objState = {
  users: [],
};

const usersReducer = (state = objState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default usersReducer;
