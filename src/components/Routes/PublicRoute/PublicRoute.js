import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useUserState } from 'context/UserContext';

const PublicRoute = ({ component, ...rest }) => {
  const { isAuthenticated } = useUserState();
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? (
          <Redirect
            to={{
              pathname: '/',
            }}
          />
        ) : (
          React.createElement(component, props)
        )
      }
    />
  );
};

export default PublicRoute;
