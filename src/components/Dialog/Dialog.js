import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog as DialogPopUp,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  DialogContentText,
} from '@material-ui/core';

const Dialog = ({ title, children, openPopup, handleClosePopUp }) => {
  return (
    <DialogPopUp open={openPopup} maxWidth='lg'>
      <DialogTitle>Checkout Form Summary</DialogTitle>
      <DialogContent dividers>
        <DialogContentText>{title}</DialogContentText>
        {children}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClosePopUp} color='primary'>
          Cancel
        </Button>
        <Button color='primary'>Update details</Button>
      </DialogActions>
    </DialogPopUp>
  );
};

Dialog.propTypes = {};

export default Dialog;
