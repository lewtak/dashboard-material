export const noWhiteSpace = (value) => {
  return /^\S{2,}$/.test(value) || 'No white space';
};

export const firstNameValid = (firstName) => {
  return firstName.length > 1 || 'Min 2 characters';
};

export const validEmailAddress = (value) => {
  return (
    /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ||
    'Enter valid email address'
  );
};
