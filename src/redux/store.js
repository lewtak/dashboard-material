import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from 'redux/reducers';
import { loadState, saveState } from 'localStorage';
import throttle from 'lodash/throttle';

const initialisedStateWithDefaults = (objState) => {
  const objInitialisedState = objState;

  return objInitialisedState;
};

export default function configureStore(objPreloadedState = {}) {
  const objInitialisedState = initialisedStateWithDefaults(objPreloadedState);

  const arrMiddlewares = [thunk];

  const enhancerMiddleware = applyMiddleware(...arrMiddlewares);

  const arrEnhancers = [enhancerMiddleware];

  const composeEnhancers = composeWithDevTools({ serialize: true });
  const composedEnhancers = composeEnhancers(...arrEnhancers);
  const persistedState = loadState();

  const store = createStore(
    rootReducer,
    persistedState,
    composedEnhancers,
    objInitialisedState
  );

  store.subscribe(
    throttle(() => {
      // const { cartState } = store.getState();
      saveState({
        // cartState,
      });
    }, 1000)
  );

  return store;
}
