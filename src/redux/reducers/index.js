import { combineReducers } from 'redux';
import usersReducer from 'redux/Users/reducer/usersReducer';
import calendarReducer from 'redux/Calendar/reducer/calendarReducer';
import reducerCheckout from 'redux/CheckoutForm/reducers/reducerCheckout';

const rootReducer = combineReducers({
  usersState: usersReducer,
  calendarState: calendarReducer,
  checkoutState: reducerCheckout,
});

export default rootReducer;
