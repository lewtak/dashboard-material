import React from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Typography,
  TextField,
  Button,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  FormHelperText,
} from '@material-ui/core';
import useStyles from './style';
import { useForm, Controller } from 'react-hook-form';
import withCheckoutForm from 'components/CheckoutForm/withCheckoutForm';
import { noWhiteSpace, firstNameValid } from 'utils/helpers/getValidationRules';

const cities = [
  {
    id: 1,
    name: 'Los Angeles',
  },
  {
    id: 2,
    name: 'Boston',
  },
  {
    id: 3,
    name: 'Saigon',
  },
];

const AddressDetails = ({
  index,
  handleGotoNextStep,
  shippingAddress,
  funcSetAddress,
}) => {
  const classes = useStyles();

  const { register, handleSubmit, errors, formState, control } = useForm({
    mode: 'onBlur',
    defaultValues: {
      firstName: shippingAddress.firstName,
      lastName: shippingAddress.lastName,
      address1: shippingAddress.address1,
      address2: shippingAddress.address2,
      city: shippingAddress.city,
    },
  });

  const onSubmitForm = (data) => {
    funcSetAddress(data);
    handleGotoNextStep(index);
  };

  return (
    <>
      <Typography variant='h4' size='sm'>
        Address Details
      </Typography>

      <form
        onSubmit={handleSubmit(onSubmitForm)}
        className={classes.formDetails}
        noValidate
        autoComplete='off'
      >
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              InputProps={{
                classes: {
                  underline: classes.textFieldUnderline,
                  input: classes.textField,
                },
              }}
              name='firstName'
              inputRef={register({
                required: 'Please fill username',
                validate: {
                  firstNameValid,
                  noWhiteSpace,
                },
              })}
              margin='normal'
              placeholder='First name'
              type='text'
              fullWidth
              label='First Name'
              error={!!errors.firstName}
              helperText={errors.firstName?.message}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              InputProps={{
                classes: {
                  underline: classes.textFieldUnderline,
                  input: classes.textField,
                },
              }}
              name='lastName'
              inputRef={register({
                required: 'Field is required',
                minLength: { value: 2, message: 'Min 2 characters' },
                maxLength: { value: 10, message: 'Max 15 caracters' },
              })}
              margin='normal'
              placeholder='Last Name'
              label='Last Name'
              type='text'
              fullWidth
              error={!!errors.lastName}
              helperText={errors.lastName?.message}
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              InputProps={{
                classes: {
                  underline: classes.textFieldUnderline,
                  input: classes.textField,
                },
              }}
              name='address1'
              inputRef={register({
                required: 'Field is required',
                minLength: { value: 2, message: 'Min 5 characters' },
              })}
              margin='normal'
              placeholder='Address 1'
              type='text'
              fullWidth
              error={!!errors.address1}
              helperText={errors.address1?.message}
            />
          </Grid>

          <Grid item xs={12} sm={12}>
            <TextField
              InputProps={{
                classes: {
                  underline: classes.textFieldUnderline,
                  input: classes.textField,
                },
              }}
              name='address2'
              inputRef={register({
                required: 'Field is required',
                minLength: { value: 2, message: 'Min 5 characters' },
              })}
              margin='normal'
              placeholder='Address 2'
              type='text'
              fullWidth
              error={!!errors.address2}
              helperText={errors.address2?.message}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id='demo-mutiple-name-label'>Cities</InputLabel>
              <Controller
                as={
                  <Select>
                    <MenuItem value='' disabled>
                      City*
                    </MenuItem>
                    {cities.map((city) => (
                      <MenuItem key={city.id} value={city.name}>
                        {city.name}
                      </MenuItem>
                    ))}
                  </Select>
                }
                name='city'
                rules={{ required: 'this is required' }}
                error={!!errors.city}
                control={control}
                defaultValue=''
              />
              <FormHelperText>{errors.city?.message}</FormHelperText>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <Button
              disabled={!formState.isValid}
              type='submit'
              variant='contained'
              color='primary'
              className={classes.button}
            >
              Next
            </Button>
          </Grid>
        </Grid>
      </form>
    </>
  );
};

AddressDetails.propTypes = {};
export default withCheckoutForm(AddressDetails);
