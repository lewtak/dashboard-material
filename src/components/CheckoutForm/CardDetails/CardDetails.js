import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Grid, Typography, TextField, Button } from '@material-ui/core';
import useStyles from './style';
import { useForm } from 'react-hook-form';
import withCheckoutForm from 'components/CheckoutForm/withCheckoutForm';
import { useHistory } from 'react-router-dom';
import { validEmailAddress } from 'utils/helpers/getValidationRules';

const CardDetails = ({
  index,
  handleGotoPreviousStep,
  handleGotoNextStep,
  shippingAddress,
  cardDetails,
  funcSetCardDetails,
}) => {
  const classes = useStyles();
  const { push } = useHistory();
  const { register, handleSubmit, errors, formState } = useForm({
    mode: 'onBlur',
    defaultValues: {
      email: cardDetails.email,
      cardNumber: cardDetails.cardNumber,
      description: cardDetails.description,
    },
  });
  const onSubmitForm = (data) => {
    handleGotoNextStep(index);
    funcSetCardDetails(data);
  };

  useEffect(() => {
    const hasAllFieldsFilled = Object.keys(shippingAddress).every(
      (fieldKey) => !!shippingAddress[fieldKey]
    );
    if (!hasAllFieldsFilled) {
      push('./shipping-address');
    }
  }, [push, shippingAddress]);
  return (
    <>
      <Typography variant='h4' size='sm'>
        Card details
      </Typography>

      <form
        onSubmit={handleSubmit(onSubmitForm)}
        className={classes.formDetails}
        noValidate
        autoComplete='off'
      >
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              InputProps={{
                classes: {
                  underline: classes.textFieldUnderline,
                  input: classes.textField,
                },
              }}
              name='email'
              margin='normal'
              label='Email address'
              placeholder='Enter your email'
              type='email'
              inputRef={register({
                required: 'Enter your e-mail',
                validate: {
                  validEmailAddress,
                },
              })}
              fullWidth
              error={!!errors.email}
              helperText={errors.email?.message}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              InputProps={{
                classes: {
                  underline: classes.textFieldUnderline,
                  input: classes.textField,
                },
              }}
              label='Card number'
              placeholder='Card Number'
              name='cardNumber'
              fullWidth
              margin='normal'
              inputRef={register({
                required: {
                  value: true,
                  message: 'Card Number is required',
                },
                minLength: {
                  value: 12,
                  message: 'Card Number 12',
                },
              })}
              error={!!errors.cardNumber}
              helperText={errors.cardNumber?.message}
            />
          </Grid>

          <Grid item xs={12} sm={12}>
            <TextField
              InputProps={{
                classes: {
                  underline: classes.textFieldUnderline,
                  input: classes.textField,
                },
              }}
              label='Description here'
              placeholder='Enter description here'
              rowsMax={4}
              name='description'
              fullWidth
              multiline
              inputRef={register({
                required: {
                  value: true,
                  message: 'Description is required',
                },
                minLength: {
                  value: 12,
                  message: 'Description Number 10',
                },
              })}
              error={!!errors.description}
              helperText={errors.description?.message}
            />
          </Grid>

          <Grid container item xs={12}>
            <Grid item xs={12} sm={6}>
              <Button
                type='submit'
                variant='contained'
                color='primary'
                className={classes.button}
                disabled={!formState.isValid}
              >
                Next
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                type='button'
                variant='contained'
                color='primary'
                className={classes.button}
                onClick={() => handleGotoPreviousStep(index)}
              >
                Prev
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </form>
    </>
  );
};

CardDetails.propTypes = {};

export default withCheckoutForm(CardDetails);
