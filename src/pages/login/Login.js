import React, { useState } from 'react';
import {
  Grid,
  CircularProgress,
  Typography,
  Button,
  Tabs,
  Tab,
  TextField,
  Fade,
} from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';

// styles
import useStyles from './style';

// logo
import logo from './logo.svg';
import google from 'images/google.svg';

// context
import { useUserDispatch, loginUser } from '../../context/UserContext';

function Login(props) {
  const classes = useStyles();

  // global
  const userDispatch = useUserDispatch();

  // local
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [activeTabId, setActiveTabId] = useState(0);

  const [loginCredentials, setLoginCredentials] = useState({
    loginValue: '',
    passwordValue: '',
    nameValue: '',
  });

  const [registerCredentials, setRegisterCredentials] = useState({
    newLoginValue: '',
    newPasswordValue: '',
    nameValue: '',
  });

  const handleLoginChange = (e) => {
    const target = e.target;
    const { name, value } = target;
    setLoginCredentials((prevCredentials) => ({
      ...prevCredentials,
      [name]: value,
    }));
  };

  const handleRegisterChange = (e) => {
    const target = e.target;
    const { name, value } = target;
    setRegisterCredentials((prevCredentials) => ({
      ...prevCredentials,
      [name]: value,
    }));
  };

  const formValidation = (loginValue, passwordValue) => ({
    loginValue: loginValue.length === 0,
    passwordValue: passwordValue.length === 0,
  });

  const registrationFormValidation = (
    newLoginValue,
    newPasswordValue,
    nameValue
  ) => ({
    newLoginValue: newLoginValue.length === 0,
    newPasswordValue: newPasswordValue.length === 0,
    nameValue: nameValue.length === 0,
  });
  const errors = formValidation(
    loginCredentials.loginValue,
    loginCredentials.passwordValue
  );

  const registrationErrors = registrationFormValidation(
    registerCredentials.newLoginValue,
    registerCredentials.newPasswordValue,
    registerCredentials.nameValue
  );

  const isDisabled = Object.keys(errors).some((x) => errors[x]);
  const registerButtonDisabled = Object.keys(registrationErrors).some(
    (x) => registrationErrors[x]
  );

  return (
    <Grid container className={classes.container}>
      <div className={classes.logotypeContainer}>
        <img src={logo} alt='logo' className={classes.logotypeImage} />
        <Typography className={classes.logotypeText}>Material Admin</Typography>
      </div>
      <div className={classes.formContainer}>
        <div className={classes.form}>
          <Tabs
            value={activeTabId}
            onChange={(e, id) => setActiveTabId(id)}
            indicatorColor='primary'
            textColor='primary'
            centered
          >
            <Tab label='Login' classes={{ root: classes.tab }} />
            <Tab label='New User' classes={{ root: classes.tab }} />
          </Tabs>
          {activeTabId === 0 && (
            <React.Fragment>
              <Typography variant='h1' className={classes.greeting}>
                Hi User
              </Typography>
              <Button size='large' className={classes.googleButton}>
                <img src={google} alt='google' className={classes.googleIcon} />
                &nbsp;Sign in with Google
              </Button>
              <div className={classes.formDividerContainer}>
                <div className={classes.formDivider} />
                <Typography className={classes.formDividerWord}>or</Typography>
                <div className={classes.formDivider} />
              </div>
              <Fade in={error}>
                <Typography color='secondary' className={classes.errorMessage}>
                  Something is wrong with your login or password :(
                </Typography>
              </Fade>
              <TextField
                id='email'
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={loginCredentials.loginValue}
                name='loginValue'
                onChange={handleLoginChange}
                margin='normal'
                placeholder='Enter your email'
                type='email'
                fullWidth
              />
              <TextField
                id='password'
                name='passwordValue'
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={loginCredentials.passwordValue}
                onChange={handleLoginChange}
                margin='normal'
                placeholder='Password'
                type='password'
                fullWidth
              />
              <div className={classes.formButtons}>
                {isLoading ? (
                  <CircularProgress size={26} className={classes.loginLoader} />
                ) : (
                  <Button
                    disabled={isDisabled}
                    onClick={() =>
                      loginUser(
                        userDispatch,
                        loginCredentials.loginValue,
                        loginCredentials.passwordValue,
                        props.history,
                        setIsLoading,
                        setError
                      )
                    }
                    variant='contained'
                    color='primary'
                    size='large'
                  >
                    Login
                  </Button>
                )}
                <Button
                  color='primary'
                  size='large'
                  className={classes.forgetButton}
                >
                  Forget Password
                </Button>
              </div>
            </React.Fragment>
          )}
          {activeTabId === 1 && (
            <React.Fragment>
              <Typography variant='h1' className={classes.greeting}>
                Welcome!
              </Typography>
              <Typography variant='h2' className={classes.subGreeting}>
                Create your account
              </Typography>
              <Fade in={error}>
                <Typography color='secondary' className={classes.errorMessage}>
                  Something is wrong with your login or password :(
                </Typography>
              </Fade>
              <TextField
                id='name'
                name='nameValue'
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={registerCredentials.nameValue}
                onChange={handleRegisterChange}
                margin='normal'
                placeholder='Full Name'
                type='text'
                fullWidth
              />
              <TextField
                id='email'
                name='newLoginValue'
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={registerCredentials.newLoginValue}
                onChange={handleRegisterChange}
                margin='normal'
                placeholder='Email Adress'
                type='email'
                fullWidth
              />
              <TextField
                id='password'
                name='newPasswordValue'
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={registerCredentials.newPasswordValue}
                onChange={handleRegisterChange}
                margin='normal'
                placeholder='Password'
                type='password'
                fullWidth
              />
              <div className={classes.creatingButtonContainer}>
                {isLoading ? (
                  <CircularProgress size={26} />
                ) : (
                  <Button
                    onClick={() =>
                      loginUser(
                        userDispatch,
                        registerCredentials.newLoginValue,
                        registerCredentials.newPasswordValue,
                        props.history,
                        setIsLoading,
                        setError
                      )
                    }
                    disabled={registerButtonDisabled}
                    size='large'
                    variant='contained'
                    color='primary'
                    fullWidth
                    className={classes.createAccountButton}
                  >
                    Create your account
                  </Button>
                )}
              </div>
              <div className={classes.formDividerContainer}>
                <div className={classes.formDivider} />
                <Typography className={classes.formDividerWord}>or</Typography>
                <div className={classes.formDivider} />
              </div>
              <Button
                size='large'
                className={classnames(
                  classes.googleButton,
                  classes.googleButtonCreating
                )}
              >
                <img src={google} alt='google' className={classes.googleIcon} />
                &nbsp;Sign in with Google
              </Button>
            </React.Fragment>
          )}
        </div>
        <Typography color='primary' className={classes.copyright}>
          © 2020 Dashboard with Calendar.
        </Typography>
      </div>
    </Grid>
  );
}

export default withRouter(Login);
