import React from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { useLayoutState } from 'context/LayoutContext';
import Header from 'components/Header';
import Sidebar from 'components/Sidebar';
import Dashboard from 'pages/dashboard';
import Calendar from 'pages/calendar';
import Tables from 'pages/tables';
import Checkout from 'pages/checkout';
// styles
import useStyles from './style';
import constants from '../../constants';

const Layout = (props) => {
  const classes = useStyles();
  const layoutState = useLayoutState();
  const { APP } = constants;
  return (
    <div className={classes.root}>
      <>
        <Header history={props.history} />
        <Sidebar />
        <div
          className={classnames(classes.content, {
            [classes.contentShift]: layoutState.isSidebarOpened,
          })}
        >
          <div className={classes.fakeToolbar} />
          <Switch>
            <Route path={APP.DASHBOARD} component={Dashboard} />
            <Route path={APP.CHECKOUT} component={Checkout} />
            <Route path={APP.CALENDAR} component={Calendar} />
            <Route path={APP.TABLES} component={Tables} />
          </Switch>
        </div>
      </>
    </div>
  );
};

export default withRouter(Layout);
