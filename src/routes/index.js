import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import PrivateRoute from 'components/Routes/PrivateRoute';
import PublicRoute from 'components/Routes/PublicRoute';
import constants from '../constants';
import Layout from 'components/Layout';
import Login from 'pages/login/Login';

const Routes = () => {
  const { ROOT, APP } = constants;
  return (
    <Switch>
      <Route exact path='/' render={() => <Redirect to='/app/dashboard' />} />
      <Route
        exact
        path={APP.ROOT}
        render={() => <Redirect to={APP.DASHBOARD} />}
      />
      <PrivateRoute path={APP.ROOT} component={Layout} />
      <PublicRoute path={ROOT.LOGIN} component={Login} />
      <Route component={Error} />
    </Switch>
  );
};

export default Routes;
