import React from 'react';
import PageTitle from 'components/PageTitle';
import PropTypes from 'prop-types';
import CalendarApp from 'components/Calendar/CalendarApp';

const Calendar = ({}) => {
  return (
    <>
      <PageTitle title='Calendar' />
      <CalendarApp />
    </>
  );
};

Calendar.propTypes = {};

export default Calendar;
