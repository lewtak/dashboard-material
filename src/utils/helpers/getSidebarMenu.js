import React from 'react';
import {
  Home as HomeIcon,
  FormatSize as TypographyIcon,
  FilterNone as UIElementsIcon,
  BorderAll as TableIcon,
  Event as EventIcon,
} from '@material-ui/icons';
import constants from '../../constants';
const { APP } = constants;
export const structure = [
  { id: 0, label: 'Dashboard', link: APP.DASHBOARD, icon: <HomeIcon /> },
  {
    id: 1,
    label: 'Checkout',
    link: APP.CHECKOUT,
    icon: <TypographyIcon />,
  },
  { id: 2, label: 'Tables', link: APP.TABLES, icon: <TableIcon /> },

  {
    id: 3,
    label: 'UI Elements',
    link: APP.UI,
    icon: <UIElementsIcon />,
    children: [
      { label: 'Icons', link: '/app/ui/icons' },
      { label: 'Charts', link: '/app/ui/charts' },
      { label: 'Maps', link: '/app/ui/maps' },
    ],
  },
  { id: 4, label: 'Calendar', link: APP.CALENDAR, icon: <EventIcon /> },
];
