import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchFakeEvents } from 'redux/Calendar/actions/actionsCalendar';

const withCalendarApp = (WrappedComponent) => {
  class HOC extends Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  const mapDispatchToProps = {
    funcFetchFakeEvents: fetchFakeEvents,
  };
  /**
   *
   * @param {object} state Redux state
   * @param {object} ownProps Props passed directly to the component
   */
  const mapStateToProps = (state, ownProps) => {
    const { events } = state.calendarState;
    return {
      ...ownProps,
      events,
    };
  };

  return connect(mapStateToProps, mapDispatchToProps)(HOC);
};

export default withCalendarApp;
