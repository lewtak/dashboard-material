import { FETCH_CALENDAR_EVENTS } from 'redux/Calendar/types';
const objState = {
  events: [],
};

const calendarReducer = (state = objState, action) => {
  switch (action.type) {
    case FETCH_CALENDAR_EVENTS:
      const { events } = action.payload;
      return {
        ...state,
        events,
      };
    default:
      return state;
  }
};

export default calendarReducer;
