import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Typography, TextField, Button } from '@material-ui/core';
import useStyles from './style';
import { useForm } from 'react-hook-form';
import withCheckoutForm from 'components/CheckoutForm/withCheckoutForm';
const OrderSummary = ({
  index,
  handleGotoPreviousStep,
  shippingAddress,
  cardDetails,
  orderSummary,
  funcSetSummaryWithNotes,
  funcSaveCheckoutDetails,
  funcSetOpenPopup,
}) => {
  const classes = useStyles();
  const { register, handleSubmit, errors, formState } = useForm({
    mode: 'onBlur',
    defaultValues: {
      notes: orderSummary.notes,
    },
  });

  const onSubmitForm = (data) => {
    funcSetSummaryWithNotes(data);
    funcSetOpenPopup();
    // funcSaveCheckoutDetails({
    //   ...shippingAddress,
    //   ...cardDetails,
    //   ...orderSummary,
    // });
  };

  return (
    <>
      <Typography variant='h4' size='sm'>
        Order summary
      </Typography>

      <form
        onSubmit={handleSubmit(onSubmitForm)}
        className={classes.formDetails}
        noValidate
        autoComplete='off'
      >
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              helperText={errors.notes?.message}
              fullWidth
              name='notes'
              margin='normal'
              label='Notes'
              multiline
              inputRef={register({
                required: {
                  value: true,
                  message: 'Notes is required',
                },
                minLength: {
                  value: 12,
                  message: 'Notes min characters 12',
                },
              })}
              rows={4}
            />
          </Grid>

          <Grid container item xs={12}>
            <Grid item xs={12} sm={6}>
              <Button
                type='submit'
                variant='contained'
                color='primary'
                className={classes.button}
                disabled={!formState.isValid}
              >
                Save data
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                type='button'
                variant='contained'
                color='primary'
                className={classes.button}
                onClick={() => handleGotoPreviousStep(index)}
              >
                Prev
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </form>
    </>
  );
};

OrderSummary.propTypes = {};

export default withCheckoutForm(OrderSummary);
