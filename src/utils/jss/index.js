import { create } from 'jss';
import { jssPreset } from '@material-ui/core/styles';
const jss = create({
  ...jssPreset(),
  // We define a custom insertion point that JSS will look for injecting the styles in the DOM.
  insertionPoint: 'jss-insertion-point',
});

export default jss;
