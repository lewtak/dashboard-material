import { ON_SUBMIT_CHECKOUT_SUMMARY } from '../types';

export const saveCheckoutDetailsToBackend = (initialValues) => (dispatch) => {
  const objCheckout = {
    ...initialValues,
  };

  console.log(objCheckout);
  dispatch({
    type: ON_SUBMIT_CHECKOUT_SUMMARY,
    payload: {
      dataSummary: objCheckout,
    },
  });
};
