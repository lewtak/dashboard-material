const style = (theme) => ({
  root: {
    '& .rbc-header': {
      padding: '12px 6px',
      fontWeight: 600,
      fontSize: 14,
    },
    '& .rbc-label': {
      padding: '8px 6px',
    },
    '& .rbc-today': {
      backgroundColor: 'transparent',
    },
    '& .rbc-header.rbc-today, & .rbc-month-view .rbc-day-bg.rbc-today': {
      borderBottom: `2px solid ${theme.palette.secondary.main}!important`,
    },
    '& .rbc-month-view, & .rbc-time-view, & .rbc-agenda-view': {
      padding: 24,
      [theme.breakpoints.down('sm')]: {
        padding: 16,
      },
      borderBottom: 0,
    },
    '& .rbc-agenda-view table': {
      '& thead > tr > th': {
        borderBottom: 0,
      },
      '& tbody > tr > td': {
        padding: '12px 6px',
        '& + td': {
          borderLeft: 1,
        },
      },
    },
    '& .rbc-agenda-table': {
      '& th': {
        border: 0,
      },
      '& th, & td': {
        padding: '12px 16px!important',
      },
    },
    '& .rbc-time-view': {
      '& .rbc-time-header': {
        borderRadius: '12px 12px 0 0',
      },
      '& .rbc-time-content': {
        flex: '0 1 auto',
      },
    },
    '& .rbc-month-view': {
      '& > .rbc-month-header': {
        borderRadius: '12px 12px 0 0',
      },
      '& > .rbc-row': {},
      '& .rbc-month-row': {
        borderWidth: '0 1px 1px 1px!important',
        minHeight: 128,
      },
      '& .rbc-header + .rbc-header': {},
      '& .rbc-header': {},
      '& .rbc-day-bg + .rbc-day-bg': {},
    },
    '& .rbc-day-slot .rbc-time-slot': {
      opacity: 0.5,
    },
    '& .rbc-time-header > .rbc-row > * + *': {},
    '& .rbc-time-content > * + * > *': {},
    '& .rbc-day-bg + .rbc-day-bg': {},
    '& .rbc-time-header > .rbc-row:first-child': {},
    '& .rbc-timeslot-group': {
      minHeight: 64,
    },
    '& .rbc-date-cell': {
      padding: 8,
      fontSize: 16,
      fontWeight: 400,
      opacity: 0.5,
      '& > a': {
        color: 'inherit',
      },
    },
    '& .rbc-event': {
      borderRadius: 4,
      padding: '4px 8px',
      backgroundColor: theme.palette.primary.dark,
      color: theme.palette.primary.contrastText,
      boxShadow: theme.shadows[0],
      transitionProperty: 'box-shadow',
      transitionDuration: theme.transitions.duration.short,
      transitionTimingFunction: theme.transitions.easing.easeInOut,
      position: 'relative',
      '&:hover': {
        boxShadow: theme.shadows[2],
      },
    },
    '& .rbc-row-segment': {
      padding: '0 4px 4px 4px',
    },
    '& .rbc-off-range-bg': {
      backgroundColor:
        theme.palette.type === 'light'
          ? 'rgba(0,0,0,0.03)'
          : 'rgba(0,0,0,0.16)',
    },
    '& .rbc-show-more': {
      color: theme.palette.secondary.main,
      background: 'transparent',
    },
    '& .rbc-addons-dnd .rbc-addons-dnd-resizable-month-event': {
      position: 'static',
    },
    '& .rbc-addons-dnd .rbc-addons-dnd-resizable-month-event .rbc-addons-dnd-resize-month-event-anchor:first-child': {
      left: 0,
      top: 0,
      bottom: 0,
      height: 'auto',
    },
    '& .rbc-addons-dnd .rbc-addons-dnd-resizable-month-event .rbc-addons-dnd-resize-month-event-anchor:last-child': {
      right: 0,
      top: 0,
      bottom: 0,
      height: 'auto',
    },
  },
  addButton: {
    position: 'absolute',
    right: 12,
    top: 172,
    zIndex: 99,
  },
});

export default style;
