import { FETCH_CALENDAR_EVENTS } from 'redux/Calendar/types';
import { fetchFakeData } from '@fake-db/db/calendar-db';

const calendarEvents = (data) => ({
  type: FETCH_CALENDAR_EVENTS,
  payload: {
    events: data,
  },
});

const fetchEvents = () => async (dispatch) => {
  const response = await fetchFakeData();
  await dispatch(calendarEvents(response));
};

export const fetchFakeEvents = () => async (dispatch) => {
  try {
    await dispatch(fetchEvents());
  } catch (error) {
    console.log(error);
  }
};
