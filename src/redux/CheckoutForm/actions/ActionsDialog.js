export const setOpenPopup = () => ({
  type: 'OPEN_DIALOG_POPUP',
  payload: {
    openPopup: true,
  },
});

export const setClosePopup = () => ({
  type: 'CLOSE_DIALOG_POPUP',
  payload: {
    openPopup: false,
  },
});
