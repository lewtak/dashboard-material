export const routes = {
  ROOT: '/',
  LOGIN: '/login',
  APP: {
    ROOT: '/app',
    DASHBOARD: '/app/dashboard',
    TABLES: '/app/tables',
    CALENDAR: '/app/calendar',
    CHECKOUT: '/app/checkout',
    TYPOGRAPHY: '/app/typography',
    UI: '/app/ui',
  },
};
