import React from 'react';
import PropTypes from 'prop-types';
import { Grid, TextField, Button } from '@material-ui/core';
import useStyles from './style';
import withCheckoutForm from 'components/CheckoutForm/withCheckoutForm';
import { useForm, Controller } from 'react-hook-form';

const CheckoutForm = ({
  shippingAddress,
  cardDetails,
  orderSummary,
  funcSaveCheckoutDetails,
  closeDialogBackToStart,
  shippingAddress: { firstName, lastName, address1, address2, city },
  orderSummary: { notes },
  cardDetails: { email, cardNumber, description },
}) => {
  const classes = useStyles();
  const { handleSubmit, reset, register } = useForm({
    defaultValues: {
      firstName,
      lastName,
      address1,
      address2,
      city,
      email,
      cardNumber,
      description,
      notes,
    },
  });

  const onSubmitForm = () => {
    funcSaveCheckoutDetails({
      ...shippingAddress,
      ...cardDetails,
      ...orderSummary,
    });

    setTimeout(() => {
      reset({
        firstName: '',
        lastName: '',
        address1: '',
        address2: '',
        city: '',
        email: '',
        cardNumber: '',
        description: '',
        notes: '',
      });
      closeDialogBackToStart();
    }, 200);
  };
  return (
    <>
      <form
        onSubmit={handleSubmit(onSubmitForm)}
        className={classes.formDetails}
        noValidate
        autoComplete='off'
      >
        <Grid container spacing={3}>
          <Grid item xs={12} sm={12}>
            <TextField
              margin='normal'
              placeholder='First name'
              label='First name'
              type='text'
              fullWidth
              variant='outlined'
              autoComplete='off'
              name='firstName'
              inputRef={register}
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              margin='normal'
              placeholder='Last name'
              label='Last name'
              type='text'
              fullWidth
              variant='outlined'
              autoComplete='off'
              name='lastName'
              inputRef={register}
            />
          </Grid>

          <Grid item xs={12} sm={12}>
            <TextField
              margin='normal'
              placeholder='City'
              label='City'
              type='text'
              fullWidth
              variant='outlined'
              autoComplete='off'
              name='city'
              inputRef={register}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              InputProps={{
                classes: {
                  underline: classes.textFieldUnderline,
                  input: classes.textField,
                },
              }}
              name='email'
              margin='normal'
              label='Email address'
              placeholder='Enter your email'
              type='email'
              inputRef={register}
              fullWidth
              variant='outlined'
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              label='Card number'
              placeholder='Card Number'
              name='cardNumber'
              fullWidth
              margin='normal'
              inputRef={register}
              variant='outlined'
            />
          </Grid>

          <Grid item xs={12} sm={12}>
            <TextField
              label='Description here'
              placeholder='Enter description here'
              name='description'
              rowsMax={4}
              fullWidth
              multiline
              inputRef={register}
              variant='outlined'
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              variant='outlined'
              fullWidth
              name='notes'
              margin='normal'
              label='Notes'
              multiline
              inputRef={register}
              rows={4}
            />
          </Grid>

          <Grid item xs={12}>
            <Button
              type='submit'
              variant='contained'
              color='primary'
              className={classes.button}
            >
              Send your details
            </Button>
          </Grid>
        </Grid>
      </form>
    </>
  );
};

CheckoutForm.propTypes = {};

export default withCheckoutForm(CheckoutForm);
