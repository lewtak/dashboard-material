import { ON_INPUT_CHANGE } from '../types';

export const checkoutInputChange = (name, value) => {
  return {
    type: ON_INPUT_CHANGE,
    payload: {
      name,
      value,
    },
  };
};
