import React, { useRef, useEffect } from 'react';
import moment from 'moment';
import withStyles from 'react-jss';
import style from './style';
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import 'react-big-calendar/lib/addons/dragAndDrop/styles.css';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import cx from 'classnames';
import { fetchFakeEvents } from 'redux/Calendar/actions/actionsCalendar';
import withCalendarApp from './withCalendarApp';
moment.locale('en-GB');

const localizer = momentLocalizer(moment);
const DragAndDropCalendar = withDragAndDrop(Calendar);
const allViews = Object.keys(Views).map((k) => Views[k]);

const CalendarApp = ({ events, funcFetchFakeEvents, classes }) => {
  const dateFormat = 'YYYY-MM-DDTHH:mm:ss.sssZ';
  const allEvents = events.map((event) => ({
    ...event,
    start: moment(event.start, dateFormat).toDate(),
    end: moment(event.end, dateFormat).toDate(),
  }));
  const headerEl = useRef(null);
  useEffect(() => {
    const fetchEvents = async () => {
      try {
        await funcFetchFakeEvents();
      } catch (error) {
        console.log(error);
      }
    };

    fetchEvents();
  }, [fetchFakeEvents]);
  console.log(allEvents);
  return (
    <div className={cx('flex flex-col flex-auto relative')}>
      <div ref={headerEl} />
      <div style={{ height: '100vh', margin: '10px' }}>
        <DragAndDropCalendar
          className='flex flex-1 container'
          selectable
          events={allEvents}
          localizer={localizer}
          resizable
          defaultView={Views.MONTH}
          defaultDate={new Date(2020, 3, 1)}
          startAccessor='start'
          endAccessor='end'
          views={allViews}
          step={60}
          showMultiDayTimes
          culture='en-GB'
        />
      </div>
    </div>
  );
};

CalendarApp.propTypes = {};

export default withCalendarApp(CalendarApp);
