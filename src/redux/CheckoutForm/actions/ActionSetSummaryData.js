export const setAddressDetails = (formData) => ({
  type: 'SET_ADDRESS_DETAILS',
  payload: {
    formData,
  },
});

export const setCardDetails = (formData) => ({
  type: 'SET_CARD_DETAILS',
  payload: {
    formData,
  },
});

export const setSummaryWithNotes = (formData) => ({
  type: 'SET_SUMMARY_NOTES',
  payload: {
    formData,
  },
});

export const saveCheckoutDetails = (summaryData) => ({
  type: 'SAVE_CHECKOUT_DETAILS',
  payload: {
    summaryData,
  },
});
